function konversiMenit(menit) {
  let jam1 = 0
  let minutes = 0
  jam = Math.floor(menit / 60)
  minutes = menit - (jam*60)
  if (minutes < 10){
    return (`${jam} : 0${minutes}`)
  } else {
    return (`${jam} : ${minutes}`)
  }
 
}

// TEST CASES
console.log(konversiMenit(63)); // 1:03
console.log(konversiMenit(124)); // 2:04
console.log(konversiMenit(53)); // 0:53
console.log(konversiMenit(88)); // 1:28
console.log(konversiMenit(120)); // 2:00